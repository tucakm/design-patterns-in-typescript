import { CopyMode, IPrototype } from "./iprototype";

export class Document implements IPrototype {
  public name: string;
  public array: [number[], number[]];

  constructor(name: string, array: [number[], number[]]) {
    this.name = name;
    this.array = array;
  }
  public clone(mode: CopyMode = "shallow"): Document {
    // This clone method uses different copy techniques
    let array;
    if (mode === "deep") {
      // results in a deep copy of the Document
      array = JSON.parse(JSON.stringify(this.array));
    } else {
      // default, results in a shallow copy of the Document
      array = Object.assign([], this.array);
    }

    return new Document(this.name, array);
  }
}
