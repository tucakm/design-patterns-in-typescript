import { Document } from "./document";
export type CopyMode = "shallow" | "deep";

export interface IPrototype {
  clone(mode: CopyMode): Document;
  // The clone, deep or shallow.
  // It is up to you how you  want to implement
  // the details in your concrete class"""
}
