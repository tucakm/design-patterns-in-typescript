import BigTable from "./big-table";
import MediumTable from "./medium-table";
import SmallTable from "./small-table";
import { ITable } from "./table";

export type TableType = "BigTable" | "MediumTable" | "SmallTable";

export class TableFactory {
  static getTable(table: TableType): ITable {
    switch (table) {
      case "SmallTable":
        return new SmallTable();
      case "MediumTable":
        return new MediumTable();
      case "BigTable":
        return new BigTable();
    }
  }
}
