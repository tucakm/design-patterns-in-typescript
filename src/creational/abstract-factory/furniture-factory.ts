import { IChair } from "./chair";
import { ChairFactory, ChairType } from "./chair-factory";
import { ITable } from "./table";
import { TableFactory, TableType } from "./table-factory";

interface IFurniture extends IChair, ITable {}
type FurnitureType = ChairType | TableType;

export class FurnitureFactory {
  public static getFurniture(furniture: FurnitureType): IFurniture | undefined {
    try {
      if (["SmallChair", "MediumChair", "BigChair"].indexOf(furniture) > -1) {
        return ChairFactory.getChair(furniture as ChairType);
      }
      if (["SmallTable", "MediumTable", "BigTable"].indexOf(furniture) > -1) {
        return TableFactory.getTable(furniture as TableType);
      }
      throw new Error("No Factory Found");
    } catch (e) {
      console.log(e);
    }
  }
}
