import ChairFactory from "./chair-factory";

const CHAIR1 = ChairFactory.getChair("SmallChair");
console.log(CHAIR1.getDimensions());

const CHAIR2 = ChairFactory.getChair("MediumChair");
console.log(CHAIR2.getDimensions());

const CHAIR3 = ChairFactory.getChair("BigChair");
console.log(CHAIR3.getDimensions());
