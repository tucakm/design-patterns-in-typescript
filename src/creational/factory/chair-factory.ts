import BigChair from "./big-chair";
import IChair from "./chair";
import MediumChair from "./medium-chair";
import SmallChair from "./small-chair";

type ChairType = "BigChair" | "MediumChair" | "SmallChair";

export default class ChairFactory {
  static getChair(chair: ChairType): IChair {
    switch (chair) {
      case "BigChair":
        return new BigChair();
      case "MediumChair":
        return new MediumChair();
      case "SmallChair":
        return new SmallChair();
    }
  }
}
