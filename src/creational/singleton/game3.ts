import { IGame } from "./igame";
import { Leaderboard } from "./leaderboard";

export class Game3 implements IGame {
  public leaderboard: Leaderboard;

  constructor() {
    this.leaderboard = new Leaderboard();
  }

  public addWinner(position: number, name: string): void {
    this.leaderboard.addWinner(position, name);
  }
}
